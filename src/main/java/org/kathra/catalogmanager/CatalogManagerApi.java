/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.catalogmanager;

import org.kathra.core.model.CatalogEntryPackage;
import org.kathra.core.model.CatalogEntryPackageVersion;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.kathra.KathraAuthRequestHandlerImpl;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.model.rest.RestBindingMode;
import static org.apache.camel.model.rest.RestParamType.*;

@ContextName("CatalogManager")
public class CatalogManagerApi extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        // configure we want to use servlet as the component for the rest DSL
        // and we enable json binding mode
        restConfiguration().component(org.kathra.iface.KathraRequestHandler.HTTP_SERVER)
        // use json binding mode so Camel automatic binds json <--> pojo
        .bindingMode(RestBindingMode.off)
        // and output using pretty print
        .dataFormatProperty("prettyPrint", "true")
        .dataFormatProperty("json.in.disableFeatures", "FAIL_ON_UNKNOWN_PROPERTIES")
        // setup context path on localhost and port number that netty will use
        .contextPath("/api/v1")
        .port("{{env:HTTP_PORT:8080}}")
        .componentProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))
        .endpointProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))
        .consumerProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))

        // add swagger api-doc out of the box
        .apiContextPath("/swagger.json")
        .apiProperty("api.title", "Kathra Catalog Manager")
        .apiProperty("api.version", "1.2.0")
        .apiProperty("api.description", "CatalogManager")
        // and enable CORS
        .apiProperty("cors", "true")
        .enableCORS(true).corsAllowCredentials(true)
        .corsHeaderProperty("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type," +
                "Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");

        rest()

        .get("/catalogEntries").outType(CatalogEntryPackage[].class).produces("application/json")
            .description("Get all entries in the catalog")
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleRequest")
                .to("bean:ReadCatalogEntriesController?method=getAllCatalogEntryPackages")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .get("/catalogEntries/{providerId}/versions/{version}").outType(CatalogEntryPackageVersion.class).produces("application/json")
            .description("Get an entry in the catalog for specific version")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("providerId")
                    .type(path)
                    .description("CatalogEntryPackage providerId")
                .endParam()
                .param()
                    .required(true)
                    .dataType("string")
                    .name("version")
                    .type(path)
                    .description("CatalogEntryPackage version")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleRequest")
                .to("bean:ReadCatalogEntriesController?method=getCatalogEntryFromVersion(${header.providerId},${header.version})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .get("/catalogEntries/{providerId}").outType(CatalogEntryPackage.class).produces("application/json")
            .description("Get an entry in the catalog")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("providerId")
                    .type(path)
                    .description("CatalogEntryPackage providerId")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleRequest")
                .to("bean:ReadCatalogEntriesController?method=getCatalogEntryPackage(${header.providerId})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest()

        .get("/catalogEntries/{providerId}/versions").outType(CatalogEntryPackageVersion[].class).produces("application/json")
            .description("Get all version for an entry in the catalog")
                .param()
                    .required(true)
                    .dataType("string")
                    .name("providerId")
                    .type(path)
                    .description("CatalogEntryPackage providerId")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleRequest")
                .to("bean:ReadCatalogEntriesController?method=getCatalogEntryPackageVersions(${header.providerId})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest();
    }
}