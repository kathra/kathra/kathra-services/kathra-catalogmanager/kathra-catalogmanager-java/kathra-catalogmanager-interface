/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.catalogmanager.service;

import org.kathra.core.model.CatalogEntryPackage;
import org.kathra.core.model.CatalogEntryPackageVersion;
import java.util.List;

public interface ReadCatalogEntriesService extends org.kathra.iface.KathraRequestHandler {

    /**
    * Get all entries in the catalog
    * 
    * @return List<CatalogEntryPackage>
    */
    List<CatalogEntryPackage> getAllCatalogEntryPackages() throws Exception;

    /**
    * Get an entry in the catalog for specific version
    * 
    * @param providerId CatalogEntryPackage providerId (required)
    * @param version CatalogEntryPackage version (required)
    * @return CatalogEntryPackageVersion
    */
    CatalogEntryPackageVersion getCatalogEntryFromVersion(String providerId, String version) throws Exception;

    /**
    * Get an entry in the catalog
    * 
    * @param providerId CatalogEntryPackage providerId (required)
    * @return CatalogEntryPackage
    */
    CatalogEntryPackage getCatalogEntryPackage(String providerId) throws Exception;

    /**
    * Get all version for an entry in the catalog
    * 
    * @param providerId CatalogEntryPackage providerId (required)
    * @return List<CatalogEntryPackageVersion>
    */
    List<CatalogEntryPackageVersion> getCatalogEntryPackageVersions(String providerId) throws Exception;

}
